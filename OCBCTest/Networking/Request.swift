//
//  Request.swift
//  OCBCTest
//
//  Created by PCCWS - User on 21/8/21.
//

import Alamofire
import SwiftyJSON

enum URLExtension: String {
    case register = "/register"
    case login = "/login"
    case balance = "/balance"
    case payees = "/payees"
    case transactions = "/transactions"
    case transfer = "/transfer"
}

public class Request : NSObject {
    static let api = Request()
    var mainURL = "https://green-thumb-64168.uc.r.appspot.com"

    func executeLogin(parameters: [String: Any], completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.login.rawValue
         let headers: HTTPHeaders = [
             "Content-Type":"application/x-www-form-urlencoded"
         ]
         AF.request(url, method: .post
             , parameters: parameters, encoding: URLEncoding.httpBody
             , headers: headers).responseJSON{(response) in
                self.printAllData(url: url, header: headers, method: "POST", parameter: parameters, response: response.result)
                switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        completionHandler(json, response.error)
                    case .failure(let error):
                        print(error)
                }
         }
    }
    
    func executeBalance(completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.balance.rawValue
        let token = KeychainManager.shared.getToken() ?? ""
        let headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded",
            "Authorization":"\(token)"
        ]
        AF.request(url, method: .get
                   , parameters: nil, encoding: URLEncoding.httpBody
             , headers: headers).responseJSON{(response) in
            self.printAllData(url: url, header: headers, method: "GET", parameter: [:], response: response.result)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completionHandler(json, response.error)
                case .failure(let error):
                    print(error)
                }
         }
    }
    
    func executePayees(completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.payees.rawValue
        let token = KeychainManager.shared.getToken() ?? ""
        let headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded",
            "Authorization":"\(token)"
        ]
        AF.request(url, method: .get
                   , parameters: nil, encoding: URLEncoding.httpBody
             , headers: headers).responseJSON{(response) in
            self.printAllData(url: url, header: headers, method: "GET", parameter: [:], response: response.result)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completionHandler(json, response.error)
                case .failure(let error):
                    print(error)
                }
         }
    }
    
    func executeTransaction(completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.transactions.rawValue
        let token = KeychainManager.shared.getToken() ?? ""
        let headers: HTTPHeaders = [
            "Content-Type":"application/x-www-form-urlencoded",
            "Authorization":"\(token)"
        ]
        AF.request(url, method: .get
                   , parameters: nil, encoding: URLEncoding.httpBody
             , headers: headers).responseJSON{(response) in
            self.printAllData(url: url, header: headers, method: "GET", parameter: [:], response: response.result)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completionHandler(json, response.error)
                case .failure(let error):
                    print(error)
                }
         }
    }
    
    func executeTransfer(parameter: [String:Any], completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.transfer.rawValue
        let token = KeychainManager.shared.getToken() ?? ""
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":"\(token)"
        ]
        AF.request(url, method: .post
                   , parameters: parameter, encoding: JSONEncoding.default
             , headers: headers).responseJSON{(response) in
            self.printAllData(url: url, header: headers, method: "POST", parameter: parameter, response: response.result)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completionHandler(json, response.error)
                case .failure(let error):
                    print(error)
                }
         }
    }
    
    func executeRegister(parameter: [String:Any], completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void){
        let url = mainURL+URLExtension.register.rawValue
        let token = KeychainManager.shared.getToken() ?? ""
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Authorization":"\(token)"
        ]
        AF.request(url, method: .post
                   , parameters: parameter, encoding: JSONEncoding.default
             , headers: headers).responseJSON{(response) in
            self.printAllData(url: url, header: headers, method: "POST", parameter: parameter, response: response.result)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completionHandler(json, response.error)
                case .failure(let error):
                    print(error)
                }
         }
    }
    
    func printAllData(url: String, header: HTTPHeaders, method: String, parameter: Any, response: Any) {
        print("\nREQUEST ")
        print("URL : \(url)")
        print("METHOD : \(method)")
        print("HEADER : \(header)")
        print("PARAMETER : \(parameter)")
        print("RESPONSE : \(response)")
    }
}
