//
//  ResponseCode.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import Foundation
import UIKit

enum LoginResponseCode {
    case success            
    case failed
}

enum BalanceResponseCode {
    case success
    case failed
}

enum TransactionResponseCode {
    case success
    case failed
}

enum PayeesResponseCode {
    case success
    case failed
}

enum TransferResponseCode {
    case success
    case failed
}

enum RegisterResponseCode {
    case success
    case failed
}

protocol Navigatable {
    /// Storyboard name where this view controller exists.
    static var storyboardName: String { get }
    
    /// Storyboard Id of this view controller.
    static var storyboardId: String { get }
    
    /// Returns a new instance created from Storyboard identifiers.
    static func instantiateFromStoryboard() -> Self
}

extension Navigatable {
    static func instantiateFromStoryboard() -> Self {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        guard
            let viewController = storyboard
                .instantiateViewController(withIdentifier: self.storyboardId) as? Self else {
            fatalError("Cannot instantiate the controller.")
        }
        
        return viewController
    }
}

extension UIViewController {
    /**
     Pushes a view controller of the provided type.
     
     - Parameter viewControllerType: Type of view controller to push.
     - Parameter completion: Function to be executed on completion.
     Contains the view controller that was pushed when successful and nil otherwise.
     */
    func pushViewControllerOfType<T: Navigatable>(viewControllerType: T.Type, completion: (T) -> Void) {
        let viewController = T.instantiateFromStoryboard()
        if let view = viewController as? UIViewController {
            self.navigationController?.pushViewController(view, animated: true)
        }
        completion(viewController)
    }
    
    func popViewControllerOfType<T: Navigatable>(viewControllerType: T.Type, completion: (T) -> Void) {
        let viewController = T.instantiateFromStoryboard()
        if let view = viewController as? UIViewController {
            self.navigationController?.popToViewController(view, animated: true)
        }
        completion(viewController)
    }
    
    /**
     Pushes a view controller of the provided type.
     
     - Parameter viewControllerType: Type of view controller to push.
     */
    func pushViewControllerOfType<T: Navigatable>(viewControllerType: T.Type) {
        self.pushViewControllerOfType(viewControllerType: viewControllerType) { _ in }
    }
    
    func popViewControllerOfType<T: Navigatable>(viewControllerType: T.Type) {
        self.popViewControllerOfType(viewControllerType: viewControllerType) { _ in }
    }
}

extension UIViewController {
    func push(controller: UIViewController, animated: Bool = true) {
        controller.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(controller, animated: animated)
    }

    func present(controller: UIViewController, animated: Bool = true) {
        controller.modalPresentationStyle = .fullScreen
        self.present(controller, animated: animated, completion: nil)
    }

    func pop(animated: Bool = true ) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func popToView(controller: UIViewController, animated: Bool = true) {
        controller.navigationItem.hidesBackButton = true
        self.navigationController?.popToViewController(controller, animated: true)
    }
}

extension UIColor {

    @nonobjc class var LightGrayLogin: UIColor {
        return UIColor(red: 221.0 / 255.0, green: 221.0 / 255.0, blue: 221.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greenNominal: UIColor {
        return UIColor(red: 52.0 / 255.0, green: 180.0 / 255.0, blue: 80.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var greyNominal: UIColor {
        return UIColor(red: 115.0 / 255.0, green: 120.0 / 255.0, blue: 116.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var redGradient1: UIColor {
        return UIColor(red: 206.0 / 255.0, green: 44.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var redGradient2: UIColor {
        return UIColor(red: 98.0 / 255.0, green: 24.0 / 255.0, blue: 19.0 / 255.0, alpha: 1.0)
    }
}

extension UIView {
    enum GradientColorDirection {
        case vertical
        case horizontal
    }
    
    /* For setting up gradients background for views
     * Parameters [Array CGColor], opacity, gradient direction: .vertical or horizontal
     * Default opacity is 1
     * Default direction is vertical
     */
    func showGradientColors(_ colors: [CGColor], opacity: Float = 1, direction: GradientColorDirection = .vertical, cornerRadius: CGFloat) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.opacity = opacity
        gradientLayer.colors = colors.map { $0 }
        
        if case .horizontal = direction {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        }
        
        gradientLayer.bounds = self.bounds
        gradientLayer.anchorPoint = CGPoint.zero
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        gradientLayer.cornerRadius = cornerRadius
        self.layer.addSublayer(gradientLayer)
    }
    
    func roundCorners(corners: UIRectCorner = [.allCorners], radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
