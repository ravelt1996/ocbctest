//
//  Constants.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import UIKit

enum StoryBoardName: String {
    case mainStoryBoard = "Main"
}

enum ViewControllerStoryBoardId: String {
    // Register Screens
    case viewController = "ViewController"
    case homeViewController = "HomeViewController"
    case transferViewController = "TransferViewController"
    case registerViewController = "RegisterViewController"
}

struct GradientColor {
    static let btnRedGradient = [UIColor.redGradient1.cgColor, UIColor.redGradient2.cgColor]
}


