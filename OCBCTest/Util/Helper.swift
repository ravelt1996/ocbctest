//
//  Helper.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import UIKit

class Helper: NSObject {
    func dateFormatter(myString: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd MMMM yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
}


