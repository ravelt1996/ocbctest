//
//  UserDefaultManager.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import Foundation

enum UserDefaultItem: String {
    case token
}

class UserDefaultManager {
    
    class func setUserDefault(value: String, forKey key: UserDefaultItem) {
        UserDefaults.standard.setValue(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func setUserDefault(value: Bool, forKey key: UserDefaultItem) {
        UserDefaults.standard.setValue(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    class func removeUserDefault(forKey key: UserDefaultItem) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func setKeyedObject(encodedData: Data, forKey key: UserDefaultItem) {
        UserDefaults.standard.set(encodedData, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    class func getUserDefault(key: UserDefaultItem) -> String? {
        return UserDefaults.standard.string(forKey: key.rawValue)
    }
    
    class func getUserDefaultBoolValue(key: UserDefaultItem) -> Bool? {
        return UserDefaults.standard.bool(forKey: key.rawValue)
    }
    
    class func getKeyedObject(key: UserDefaultItem) -> Data? {
        return UserDefaults.standard.data(forKey: key.rawValue)
    }
}


