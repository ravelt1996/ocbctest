//
//  KeychainManager.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import Foundation
import KeychainAccess

enum KeychainItem: String {
    case applicationId    = "com.sonnyescape.OCBCTest"
}

class KeychainManager {
    static let shared: KeychainManager = {
        let instance = KeychainManager()
        
        return instance
    }()
    
    let keychain = Keychain(service: KeychainItem.applicationId.rawValue)
    
    func deleteAllSavedData() {
        do {
            try keychain.removeAll()
        } catch let error {
            NSLog("Error occured while clearing data: \(error)")
        }
    }
    
    func deleteToken() {
        UserDefaultManager.removeUserDefault(forKey: .token)
    }
    
    func deleteKey(key: KeychainItem) {
        do {
            try keychain.remove(key.rawValue)
        } catch let error {
            NSLog("Error occured while clearing data: \(error)")
        }
    }
    
    func setToken(_ token: String, forKey: UserDefaultItem) {
        UserDefaultManager.setUserDefault(value: token, forKey: forKey)
    }
    
    func getToken(forKey: UserDefaultItem) -> String? {
        return UserDefaultManager.getUserDefault(key: forKey)
    }
    
    func getToken() -> String? {
        return UserDefaultManager.getUserDefault(key: .token)
    }
    
    func getBasicToken() -> String? {
        if let token = UserDefaultManager.getUserDefault(key: .token) {
            return "Basic "+token
        }
        return ""
    }
    
    func getBearerToken() -> String? {
        if let token = UserDefaultManager.getUserDefault(key: .token) {
            return "Bearer "+token
        }
        return ""
    }
    
    // Getters and setters
    func saveString(value: String, forkey key: KeychainItem) {
        keychain[key.rawValue] = value
    }
    
    func getString(key: KeychainItem) -> String? {
        return keychain[key.rawValue]
    }
}
