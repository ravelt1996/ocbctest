//
//  TransactionTableViewCell.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var accountNameLbl: UILabel!
    @IBOutlet weak var accountNoLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var backView: UIView!
    var transactionInfo: TransactionInfoModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupView() {
        self.backView.layer.cornerRadius = 10.0
        accountNameLbl.text = transactionInfo?.account?.accountHolder
        accountNoLbl.text = transactionInfo?.account?.accountNo
        if let amount = transactionInfo?.amount {
            let nominal = formatToIndonesianCurrencyFormat(value: amount)
            if amount > 0 {
                numberLbl.textColor = UIColor.greenNominal
            } else if amount < 0 {
                numberLbl.textColor = UIColor.red
            } else {
                numberLbl.textColor = UIColor.greyNominal
            }
            numberLbl.text = nominal
        }
    }
    
    func formatToIndonesianCurrencyFormat(value: Int) -> String {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.groupingSize = 3
        formatter.groupingSeparator = "."
        formatter.decimalSeparator = ","
        return formatter.string(from: value as NSNumber) ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
