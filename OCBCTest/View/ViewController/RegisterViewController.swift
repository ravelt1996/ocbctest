//
//  RegisterViewController.swift
//  OCBCTest
//
//  Created by PCCWS - User on 29/12/21.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confPasswordTxt: UITextField!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var confirmPasswordView: UIView!
    @IBOutlet weak var btnBackView: UIView!
    let registerViewModel = RegisterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        usernameView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        passwordView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        confirmPasswordView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        usernameView.layer.borderWidth = 1
        passwordView.layer.borderWidth = 1
        confirmPasswordView.layer.borderWidth = 1
        usernameView.layer.cornerRadius = 5
        passwordView.layer.cornerRadius = 5
        confirmPasswordView.layer.cornerRadius = 5
        btnBackView.showGradientColors(GradientColor.btnRedGradient, opacity: 1.0, direction: .horizontal, cornerRadius: 22.5)
        btnBackView.layer.cornerRadius = 15
        passwordTxt.isSecureTextEntry = true
        confPasswordTxt.isSecureTextEntry = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
       executeRegister()
    }
    
    func executeRegister() {
        guard let username = usernameTxt.text, let password = passwordTxt.text, let confPassword = confPasswordTxt.text, password == confPassword else { return }
        let user = UserRegisterModel.init(username: username, password: password)
        self.registerViewModel.registerUser(user: user) { error in
            if error == .success {
                self.pop()
            }
        }
    }
    
}

extension RegisterViewController: Navigatable {
    static var storyboardName: String {
        return StoryBoardName.mainStoryBoard.rawValue
    }
    
    static var storyboardId: String {
        return ViewControllerStoryBoardId.registerViewController.rawValue
    }
}
