//
//  ViewController.swift
//  OCBCTest
//
//  Created by PCCWS - User on 27/12/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var usernameErrorLbl: UILabel!
    @IBOutlet weak var passwordErrorLbl: UILabel!
    @IBOutlet weak var btnBackView: UIView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    let loginViewModel = LoginViewModel()
    var loginInfo: LoginInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setupView() {
        usernameView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        passwordView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        usernameView.layer.borderWidth = 1
        passwordView.layer.borderWidth = 1
        usernameView.layer.cornerRadius = 5
        passwordView.layer.cornerRadius = 5
        btnBackView.showGradientColors(GradientColor.btnRedGradient, opacity: 1.0, direction: .horizontal, cornerRadius: 22.5)
        passwordTxt.isSecureTextEntry = true
    }

    @IBAction func btnLoginClicked(_ sender: UIButton) {
        executeLogin()
    }
    
    @IBAction func btnRegister(_ sender: UIButton) {
        moveToRegister()
    }
    
    func executeLogin() {
        guard let username = usernameTxt.text, username.isEmpty == false, let password = passwordTxt.text, password.isEmpty == false else {
            self.showError(error: "Username and password cannot be empty")
            return
        }
        loginViewModel.getToken(username: username, password: password, completionHandler: { responseCode in
            if responseCode == LoginResponseCode.success {
                self.loginInfo = self.loginViewModel.loginInfo
                self.moveToHome()
            } else {
                self.showError(error: self.loginViewModel.loginInfo?.error ?? "")
            }
        })
    }
    
    func moveToHome() {
        self.pushViewControllerOfType(viewControllerType: HomeViewController.self) { vc in
            vc.loginInfoModel = self.loginInfo
        }
    }
    
    func moveToRegister() {
        self.pushViewControllerOfType(viewControllerType: RegisterViewController.self)
    }
    
    func showError(error: String) {
        usernameErrorLbl.text = error
        passwordErrorLbl.text = error
    }
    
}

