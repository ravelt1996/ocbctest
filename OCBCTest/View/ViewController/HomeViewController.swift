//
//  HomeViewController.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var accountNoLbl: UILabel!
    @IBOutlet weak var accountHolderLbl: UILabel!
    @IBOutlet weak var transTableView: UITableView!
    @IBOutlet weak var btnBackView: UIView!
    let homeViewModel = HomeViewModel()
    var balanceInfo: BalanceInfoModel?
    var loginInfoModel: LoginInfoModel?
    var arrTransInfo: [TransactionCellModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.getBalance()
        self.getTransaction()
        self.tableInitiate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupView() {
        btnBackView.showGradientColors(GradientColor.btnRedGradient, opacity: 1.0, direction: .horizontal, cornerRadius: 22.5)
    }
    
    func getBalance() {
        homeViewModel.getBalance { responseCode in
            if responseCode == .success {
                self.balanceInfo = self.homeViewModel.balanceInfo
                self.refreshView()
            }
        }
    }
    
    func getTransaction() {
        homeViewModel.getTransaction { responseCode in
            if responseCode == .success {
                self.arrTransInfo = self.homeViewModel.arrTransactionCell
                self.transTableView.reloadData()
            }
        }
    }
    
    func tableInitiate() {
        self.transTableView.delegate = self
        self.transTableView.dataSource = self
        self.transTableView.register(UINib(nibName: "TransactionTableViewCell", bundle: nil), forCellReuseIdentifier: "transCell")
        self.transTableView.register(UINib(nibName: "DateTableViewCell", bundle: nil), forCellReuseIdentifier: "dateCell")
        self.transTableView.separatorColor = .clear
    }
    
    func refreshView() {
        accountNoLbl.text = self.balanceInfo?.accountNo
        accountHolderLbl.text = self.loginInfoModel?.username
        balanceLbl.text = formatToIndonesianCurrencyFormat(value: balanceInfo?.balance ?? 0)
    }
    
    func formatToIndonesianCurrencyFormat(value: Int) -> String {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.groupingSize = 3
        formatter.groupingSeparator = "."
        formatter.decimalSeparator = " "
        return formatter.string(from: value as NSNumber) ?? ""
    }
    
    @IBAction func btnTransferClicked(_ sender: UIButton) {
        self.pushViewControllerOfType(viewControllerType: TransferViewController.self)
    }
    
    @IBAction func btnSignOutClicked(_ sender: UIButton) {
        KeychainManager().deleteToken()
        self.pop()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTransInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = arrTransInfo[indexPath.row]
        if item.header == true {
            let cell = transTableView.dequeueReusableCell(withIdentifier: "dateCell") as! DateTableViewCell
            cell.dateLabel.text = item.date
            return cell
        } else if item.header == false {
            let cell = transTableView.dequeueReusableCell(withIdentifier: "transCell") as! TransactionTableViewCell
            cell.transactionInfo = item.transaction
            cell.setupView()
            return cell
        }
        return UITableViewCell()
    }
}

extension HomeViewController: Navigatable {
    static var storyboardName: String {
        return StoryBoardName.mainStoryBoard.rawValue
    }
    
    static var storyboardId: String {
        return ViewControllerStoryBoardId.homeViewController.rawValue
    }
}
