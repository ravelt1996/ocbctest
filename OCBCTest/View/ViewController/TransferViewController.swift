//
//  TransferViewController.swift
//  OCBCTest
//
//  Created by PCCWS - User on 29/12/21.
//

import UIKit

class TransferViewController: UIViewController {

    @IBOutlet weak var transferView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var payeesTxt: UITextField!
    @IBOutlet weak var amountTxt: UITextField!
    @IBOutlet weak var descTxt: UITextField!
    @IBOutlet weak var btnBackView: UIView!
    let transferViewModel = TransferViewModel()
    var transferTo: TransferToModel?
    var arrPayees: [PayeesModel] = []
    var target: PayeesModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
    }
    
    func setupView() {
        transferViewModel.getPayees { error in
            self.arrPayees = self.transferViewModel.arrPayees
            self.createPickerView()
        }
        transferView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        amountView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        descriptionView.layer.borderColor = UIColor.LightGrayLogin.cgColor
        transferView.layer.borderWidth = 1
        amountView.layer.borderWidth = 1
        descriptionView.layer.borderWidth = 1
        transferView.layer.cornerRadius = 5
        amountView.layer.cornerRadius = 5
        descriptionView.layer.cornerRadius = 5
        btnBackView.showGradientColors(GradientColor.btnRedGradient, opacity: 1.0, direction: .horizontal, cornerRadius: 22.5)
    }
    
    @IBAction func btnTransferClicked(_ sender: UIButton) {
        guard let id = target?.accountNo, let amount = amountTxt.text, let desc = descTxt.text else { return }
        let intAmount = Int(amount) ?? 0
        transferTo = TransferToModel.init(accountNo: id, amount: intAmount, desc: desc)
        transferFund()
    }
    
    func transferFund() {
        if let transferItem = transferTo {
            transferViewModel.transferFund(transferTo: transferItem) { error in
                self.pop()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TransferViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func createPickerView() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        payeesTxt.inputView = pickerView
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        toolBar.isUserInteractionEnabled = true
        payeesTxt.inputAccessoryView = toolBar
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrPayees.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrPayees[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        payeesTxt.text = self.arrPayees[row].name
        target = self.arrPayees[row]
    }
}

extension TransferViewController: Navigatable {
    static var storyboardName: String {
        return StoryBoardName.mainStoryBoard.rawValue
    }
    
    static var storyboardId: String {
        return ViewControllerStoryBoardId.transferViewController.rawValue
    }
}
