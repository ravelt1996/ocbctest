//
//  LoginInfoModel.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import UIKit
import SwiftyJSON

class LoginInfoModel: NSObject {
    var accountNo: String?
    var status: String?
    var token: String?
    var username: String?
    var error: String?
    
    init(dict: JSON) {
        username = dict["username"].string
        accountNo = dict["accountNo"].string
        status = dict["status"].string
        token = dict["token"].string
        error = dict["error"].string
        if token?.isEmpty == false {
            KeychainManager.shared.setToken(token!, forKey: .token)
        }
    }
}

class BalanceInfoModel: NSObject {
    var accountNo: String?
    var balance: Int?
    var status: String?
    
    init(dict: JSON) {
        accountNo = dict["accountNo"].string
        balance = dict["balance"].int
        status = dict["status"].string
    }
}

class TransactionInfoModel: NSObject {
    var amount: Int?
    var desc: String?
    var transactionDate: String?
    var transactionId: String?
    var transactionType: String?
    var account: AccountModel?
    var convertedDate: String?
    
    init(dict: JSON) {
        amount = dict["amount"].int
        desc = dict["description"].string
        transactionDate = dict["transactionDate"].string
        transactionId = dict["transactionId"].string
        transactionType = dict["transactionType"].string
        convertedDate = Helper().dateFormatter(myString: transactionDate ?? "")
        let dataRecipient = dict["receipient"] as JSON
        if dataRecipient.isEmpty == false {
            account = AccountModel.init(dict: dataRecipient)
        }
    }
}

class TransactionCellModel: NSObject {
    var date: String?
    var transaction: TransactionInfoModel?
    var header: Bool?
    
    override init() {
        date = ""
        header = true
    }
}

class AccountModel: NSObject {
    var accountHolder: String?
    var accountNo: String?
    
    init(dict: JSON) {
        accountHolder = dict["accountHolder"].string
        accountNo = dict["accountNo"].string
    }
}

class PayeesModel: NSObject {
    var accountNo: String?
    var id: String?
    var name: String?
    init(dict: JSON) {
        accountNo = dict["accountNo"].string
        id = dict["id"].string
        name = dict["name"].string
    }
}

class TransferToModel: NSObject {
    var receipientAccountNo: String?
    var amountTotal: Int?
    var descTransfer: String?
    init(accountNo: String, amount: Int, desc: String) {
        receipientAccountNo = accountNo
        amountTotal = amount
        descTransfer = desc
    }
}

class UserRegisterModel: NSObject {
    var username: String?
    var password: String?
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
}

class BaseResponseModel: NSObject {
    var status: String?
    var data: JSON
    init(dict: JSON) {
        status = dict["status"].string
        data = dict["data"] as JSON
    }
}
