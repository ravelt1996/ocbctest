//
//  LoginViewModel.swift
//  OCBCTest
//
//  Created by PCCWS - User on 27/12/21.
//

import Foundation
import SwiftyJSON

class LoginViewModel: NSObject {
    var loginInfo: LoginInfoModel?
    
    public func getToken(username: String, password: String, completionHandler: @escaping (_ response: LoginResponseCode?) -> Void) {
        let body : [String : Any] = [
            "username": username,
            "password": password
        ]
        Request.api.executeLogin(parameters: body ) { result, error in
            self.loginInfo = LoginInfoModel.init(dict: result)
            if self.loginInfo?.status == "success" {
                completionHandler(LoginResponseCode.success)
            } else {
                completionHandler(LoginResponseCode.failed)
            }
        }
    }
}

