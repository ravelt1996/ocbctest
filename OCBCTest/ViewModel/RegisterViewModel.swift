//
//  RegisterViewModel.swift
//  OCBCTest
//
//  Created by PCCWS - User on 29/12/21.
//

import Foundation
import SwiftyJSON

class RegisterViewModel: NSObject {

    public func registerUser(user: UserRegisterModel, completionHandler: @escaping (_ error: RegisterResponseCode?) -> Void) {
        let body : [String : Any] = [
            "username": user.username ?? "",
            "password": user.password ?? ""
        ]
        Request.api.executeRegister(parameter: body) { result, error in
            let info = BaseResponseModel.init(dict: result)
            if info.status == "success" {
                completionHandler(RegisterResponseCode.success)
            } else {
                completionHandler(RegisterResponseCode.failed)
            }
        }
    }
}




