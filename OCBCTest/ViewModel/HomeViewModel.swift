//
//  LoginViewModel.swift
//  OCBCTest
//
//  Created by PCCWS - User on 28/12/21.
//

import Foundation
import SwiftyJSON

class HomeViewModel: NSObject {
    var balanceInfo: BalanceInfoModel?
    var arrTransactionInfo: [TransactionInfoModel] = []
    var arrTransactionCell: [TransactionCellModel] = []
    public func getBalance(completionHandler: @escaping (_ error: BalanceResponseCode?) -> Void) {
        Request.api.executeBalance { result, error in
            self.balanceInfo = BalanceInfoModel.init(dict: result)
            if self.balanceInfo?.status == "success" {
                completionHandler(BalanceResponseCode.success)
            } else {
                completionHandler(BalanceResponseCode.failed)
            }
        }
    }
    
    public func getTransaction(completionHandler: @escaping (_ error: TransactionResponseCode?) -> Void) {
        Request.api.executeTransaction { result, error in
            let info = BaseResponseModel.init(dict: result)
            if info.status == "success" {
                self.arrTransactionInfo.removeAll()
                if let array = info.data.array {
                    for item in array {
                        let newTrans = TransactionInfoModel.init(dict: item)
                        self.arrTransactionInfo.append(newTrans)
                    }
                }
                self.sortArrTrans()
                completionHandler(TransactionResponseCode.success)
            } else {
                completionHandler(TransactionResponseCode.failed)
            }
        }
    }
    
    private func sortArrTrans() {
        arrTransactionInfo = arrTransactionInfo.sorted {
            $0.transactionDate ?? "" > $1.transactionDate ?? ""
        }
        var date = ""
        if arrTransactionInfo.isEmpty == false {
            date = arrTransactionInfo[0].convertedDate ?? ""
            let transDate = TransactionCellModel.init()
            transDate.date = date
            transDate.header = true
            let transCell = TransactionCellModel.init()
            transCell.transaction = arrTransactionInfo[0]
            transCell.header = false
            arrTransactionCell.append(transDate)
            arrTransactionCell.append(transCell)
            for item in arrTransactionInfo {
                if date != item.convertedDate {
                    date = item.convertedDate ?? ""
                    let transDate = TransactionCellModel.init()
                    transDate.date = date
                    transDate.header = true
                    let transCell = TransactionCellModel.init()
                    transCell.transaction = arrTransactionInfo[0]
                    transCell.header = false
                    arrTransactionCell.append(transDate)
                    arrTransactionCell.append(transCell)
                } else if date == item.convertedDate {
                    let transCell = TransactionCellModel.init()
                    transCell.transaction = item
                    transCell.header = false
                    arrTransactionCell.append(transCell)
                }
            }
        }
    }
}


