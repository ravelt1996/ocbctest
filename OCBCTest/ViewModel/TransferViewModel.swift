//
//  TransferViewModel.swift
//  OCBCTest
//
//  Created by PCCWS - User on 29/12/21.
//

import Foundation
import SwiftyJSON

class TransferViewModel: NSObject {
    var arrPayees: [PayeesModel] = []
    
    public func getPayees(completionHandler: @escaping (_ error: PayeesResponseCode?) -> Void) {
        Request.api.executePayees { result, error in
            let info = BaseResponseModel.init(dict: result)
            if info.status == "success" {
                self.arrPayees.removeAll()
                if let array = info.data.array {
                    for item in array {
                        let newPayees = PayeesModel.init(dict: item)
                        self.arrPayees.append(newPayees)
                    }
                }
                completionHandler(PayeesResponseCode.success)
            } else {
                completionHandler(PayeesResponseCode.failed)
            }
        }
    }
    
    public func transferFund(transferTo: TransferToModel, completionHandler: @escaping (_ error: TransferResponseCode?) -> Void) {
        let body : [String : Any] = [
            "receipientAccountNo": transferTo.receipientAccountNo ?? "",
                "amount": transferTo.amountTotal ?? 0,
                "description": transferTo.descTransfer ?? ""
        ]
        Request.api.executeTransfer(parameter: body) { result, error in
            let info = BaseResponseModel.init(dict: result)
            if info.status == "success" {
                completionHandler(TransferResponseCode.success)
            } else {
                completionHandler(TransferResponseCode.failed)
            }
        }
    }
}



