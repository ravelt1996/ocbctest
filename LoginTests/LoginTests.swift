//
//  LoginTests.swift
//  LoginTests
//
//  Created by PCCWS - User on 30/12/21.
//

import XCTest
@testable import OCBCTest

class LoginTests: XCTestCase {
    
    var loginViewModel: LoginViewModel?
    var viewController: ViewController?
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        viewController = nil
        loginViewModel = nil
        try super.tearDownWithError()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testValidLoginCredentials() {
        loginViewModel?.getToken(username: "test", password: "asdasd") { response in
            print(response)
        }
    }
    
    func testButtonLogin() {
        viewController?.btnLogin.sendActions(for: .touchUpInside)
    }
    
    func testButtonRegister() {
        viewController?.btnRegister.sendActions(for: .touchUpInside)
    }
    
    func testExecuteFunctions() {
        viewController?.executeLogin()
        viewController?.moveToRegister()
        viewController?.moveToHome()
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
